# Test app

[Run online](http://bell-integrator.surge.sh)

## Setup

```
$ make install
```

## Run

```
$ make run
```

## Build

```
$ make build
```

## Deploy

```
$ make deploy
```

## Test

```
$ make test
```

## Eslint

```
$ make lint
```
