import React, { Component } from "react";
import "./Login.css";

export default class Login extends Component {
  handleSubmit = e => {
    e.preventDefault();
    this.props.getLogin().then(() => this.props.history.push("/companies"));
  };

  handleChange = e => {
    const { id, value } = e.target;
    this.props.addUserData({ type: "LOGIN", [id]: value });
  };

  render() {
    return (
      <div className="text-center">
        <form className="form-signin" onSubmit={this.handleSubmit}>
          <h1 className="h3 mb-3 font-weight-normal">Please, log in</h1>
          <label htmlFor="inputEmail" className="sr-only">
            Email address
          </label>
          <input
            type="email"
            id="inputEmail"
            className="form-control"
            placeholder="Email address"
            required
            autoFocus
            title={"Пропускаю всех!"}
            onChange={this.handleChange}
          />
          <label htmlFor="inputPassword" className="sr-only">
            Password
          </label>
          <input
            type="password"
            id="inputPassword"
            className="form-control"
            placeholder="Password"
            title={"Пропускаю всех!"}
            required
            onChange={this.handleChange}
          />
          <button
            className="btn btn-lg btn-primary btn-block"
            >
            Log in
          </button>
        </form>
      </div>
    );
  }
}
