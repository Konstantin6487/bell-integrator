import React, { Component } from "react";
import { ListGroup, ListGroupItem } from "react-bootstrap";

export default class Employers extends Component {
  render() {
    const { employers } = this.props;
    const currCompanyEmployers = employers.filter(
      ({ unitId }) => unitId === Number(this.props.match.params.unitId)
    ).slice().sort((a, b) => a.surname.localeCompare(b.surname));
    return (
      <div>
        <h2>Сотрудники</h2>

        <ListGroup>
          {currCompanyEmployers.map(({ name, surname, patronymic, id }) => (
            <ListGroupItem key={id}>
              <p>
                {surname} {name} {patronymic}
              </p>
            </ListGroupItem>
          ))}
        </ListGroup>

        <hr />
      </div>
    );
  }
}

//<Link to={`${match.url}/${id}`}></Link>
