import React, { Component } from "react";
import { LinkContainer } from "react-router-bootstrap";
import "./Home.css";

export default class Home extends Component {
  render() {
    return (
      <div className="Home">
        <div className="lander">
          <h1>Welcome!</h1>
          <LinkContainer to="/login">
            <a>Please, log in</a>
          </LinkContainer>
        </div>
      </div>
    );
  }
}
