import React, { Component } from "react";
import { Link } from "react-router-dom";
import { ListGroup, ListGroupItem } from "react-bootstrap";

export default class Companies extends Component {
  render() {
    const { computedMatch: match, companies } = this.props;
    return (
      <div>
        <h2>Компании</h2>
        <ListGroup>
          {companies.map(({ title, id }) => (
            <ListGroupItem key={id}>
              <Link to={`${match.url}/${id}`}>{title}</Link>
            </ListGroupItem>
          ))}
        </ListGroup>
        <hr />
      </div>
    );
  }
}
