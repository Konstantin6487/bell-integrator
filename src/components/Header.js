import React, { Component } from "react";
import { Nav, Navbar, NavItem } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import Routes from "../Routes";
import "./Header.css";

export default class Header extends Component {
  state = {
    defaultPath: "/login"
  };

  onLogout = () => {
    this.props.loginOut();
  };

  render() {
    const { defaultPath } = this.state;
    const { loginStatus } = this.props;

    return (
      <div className="Header container">
        <Navbar fluid collapseOnSelect>
          <Navbar.Header>
            <Navbar.Brand>
              <span>Bell integrator</span>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav pullRight>
              {loginStatus ? (
                <LinkContainer to="/companies">
                  <NavItem>Companies</NavItem>
                </LinkContainer>
              ) : null}
              {loginStatus ? (
                <LinkContainer to={defaultPath}>
                  <NavItem onClick={this.onLogout}>Logout</NavItem>
                </LinkContainer>
              ) : (
                <LinkContainer to={defaultPath}>
                  <NavItem>Login</NavItem>
                </LinkContainer>
              )}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Routes loginStatus={loginStatus} defaultPath={defaultPath} />
      </div>
    );
  }
}
