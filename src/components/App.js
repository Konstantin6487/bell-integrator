import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import Header from "../containers/Header";
import store from "../store";

export default () => (
  <Provider store={store}>
    <Router>
      <Header />
    </Router>
  </Provider>
);
