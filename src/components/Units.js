import React, { Component } from "react";
import { Link } from "react-router-dom";
import { ListGroup, ListGroupItem } from "react-bootstrap";

export default class Units extends Component {
  render() {
    const { match, units } = this.props;
    const currCompanyUnit = units.filter(
      ({ compId }) => compId === Number(match.params.compId)
    );
    return (
      <div>
        <h2>Подразделения</h2>
        <ListGroup>
          {currCompanyUnit.map(({ title, id }) => (
            <ListGroupItem key={id}>
              <Link to={`${match.url}/${id}`}>{title}</Link>
            </ListGroupItem>
          ))}
        </ListGroup>
        <hr />
      </div>
    );
  }
}
