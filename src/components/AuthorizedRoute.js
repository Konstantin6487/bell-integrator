import React, { Component } from "react";
import { Redirect, Route } from "react-router-dom";

export default class AuthorizedRoute extends Component {
  render() {
    const { component: Component, loginStatus, loading, ...rest } = this.props;
    return (
      <Route
        {...rest}
        render={props => {
          if (loading) {
            return <div>{"Loading... please wait"}</div>;
          }
          return loginStatus ? (
            <Component {...this.props} />
          ) : (
            <Redirect to="/login" />
          );
        }}
      />
    );
  }
}
