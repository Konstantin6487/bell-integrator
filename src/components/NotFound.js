import React from "react";

export default () => (
  <div>
    <hr />
    <br />
    <h2 style={{textAlign: "center"}}>Sorry, page not found!</h2>
  </div>
);
