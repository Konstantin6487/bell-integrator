import React from "react";
import { Route, Switch } from "react-router-dom";
import AuthorizedRoute from "./containers/AuthorizedRoute";
import Companies from "./containers/Companies";
import Employers from "./containers/Employers";
import Home from "./components/Home";
import Login from "./containers/Login";
import NotFound from "./components/NotFound";
import Units from "./containers/Units";

export default props => {
  const { loginStatus, defaultPath } = props;

  return (
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path={defaultPath} component={Login} />
      <Route
        path={loginStatus ? "/companies/:compId/:unitId" : defaultPath}
        component={loginStatus ? Employers : Login}
      />
      <Route
        path={loginStatus ? "/companies/:compId" : defaultPath}
        component={loginStatus ? Units : Login}
      />

      <AuthorizedRoute path="/companies" component={Companies} />
      <Route component={NotFound} />
    </Switch>
  );
};
