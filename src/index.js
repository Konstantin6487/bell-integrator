import React from "react";
import { render } from "react-dom";
import App from "./components/App";
import "bootstrap3/dist/css/bootstrap.min.css";
import "./index.css";
import registerServiceWorker from "./registerServiceWorker";

render(<App />, document.getElementById("root"));
registerServiceWorker();
