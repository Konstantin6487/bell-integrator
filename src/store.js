import { createStore, applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import reducers from "./reducers";

const reduxDevtools = window.__REDUX_DEVTOOLS_EXTENSION__;

export default createStore(reducers, reduxDevtools && reduxDevtools(), applyMiddleware(thunk));
