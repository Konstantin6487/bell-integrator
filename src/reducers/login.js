import { initStateLogin } from "./initstates";

export default (state = initStateLogin, action) => {
  switch (action.type) {
    case "LOGIN_REQUEST":
      return { ...state, loading: true };
    case "LOGIN_SUCCESS":
      return { ...state, loading: false, loginStatus: true };
    case "USERDATA_ADD":
      return { ...state, ...action.payload };
    case "LOGIN_OUT":
      return { ...state, loginStatus: false };
    default:
      return state;
  }
};
