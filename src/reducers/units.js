import { initStateUnits } from "./initstates";

export default (state = initStateUnits, { type, payload }) => {
  switch (type) {
    case "UNIT_ADD":
      let newCompanyId =
        state.length === 0 ? 1 : Math.max(...state.map(({ id }) => id)) + 1;
      return [...state, { id: newCompanyId, ...payload }];
    case "UNIT_REMOVE":
      return state.filter(({ id }) => id !== payload.id);
    case "UNIT_UPDATE":
      const renewCompany = state.find(({ id }) => id === payload.id);
      return !renewCompany
        ? state
        : [...state, { ...renewCompany, ...payload }];
    default:
      return state;
  }
};
