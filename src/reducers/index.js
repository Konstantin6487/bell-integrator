import { combineReducers } from "redux";
import login from "./login";
import companies from "./companies";
import units from "./units";
import employers from "./employers";

export default combineReducers({ login, companies, units, employers });
