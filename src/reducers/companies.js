import { initStateCompanies } from "./initstates";

export default (state = initStateCompanies, { type, payload }) => {
  switch (type) {
    case "COMPANY_ADD":
      let newCompanyId =
        state.length === 0 ? 1 : Math.max(...state.map(({ id }) => id)) + 1;
      return [...state, { id: newCompanyId, ...payload }];
    case "COMPANY_REMOVE":
      return state.filter(({ id }) => id !== payload.id);
    case "COMPANY_UPDATE":
      const renewCompany = state.find(({ id }) => id === payload.id);
      return !renewCompany
        ? state
        : [...state, { ...renewCompany, ...payload }];
    default:
      return state;
  }
};
