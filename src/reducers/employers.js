import { initStateEmployers } from "./initstates";

export default (state = initStateEmployers , { type, payload }) => {
  switch (type) {
    case "EMPLOYER_ADD":
      let newEmployerId =
        state.length === 0 ? 1 : Math.max(...state.map(({ id }) => id)) + 1;
      return [...state, { id: newEmployerId, ...payload }];
    case "EMPLOYER_REMOVE":
      return state.filter(({ id }) => id !== payload.id);
    case "EMPLOYER_UPDATE":
      const renewEmployer = state.find(({ id }) => id === payload.id);
      return !renewEmployer
        ? state
        : [...state, { ...renewEmployer, ...payload }];
    default:
      return state;
  }
};
