export const initStateLogin = {
  surname: "",
  pass: "",
  loginStatus: false,
  loading: false
};

export const initStateCompanies = [
  {
    id: 1,
    title: "АНО СОЭКС-ВОЛГА",
    address: "Г САРАТОВ,УЛ МАРКИНА, Д 1",
    itn: "6452034006"
  },
  { id: 2, title: "АНО Учебный центр Трайтек", address: "", itn: "6452046178" },
  {
    id: 3,
    title: "АНО ХОККЕЙНЫЙ КЛУБ УНИВЕРСАЛ",
    address: "Г САРАТОВ,УЛ РАДИЩЕВА, Д 22, СТАДИОН ДИНАМО",
    itn: "6455032409"
  },
  {
    id: 4,
    title: "АНО Центр АСТРА",
    address: "САРАТОВСКАЯ ОБЛ, САРАТОВ Г, ИМ ДЗЕРЖИНСКОГО Ф.Э. УЛ, 23, 2",
    itn: "6455050415"
  }
];

export const initStateUnits = [
  { id: 1, compId: 1, title: "Бухгалтерия" },
  { id: 2, compId: 2, title: "Бухгалтерия" },
  { id: 3, compId: 3, title: "Бухгалтерия" },
  { id: 4, compId: 4, title: "Бухгалтерия" },

  { id: 5, compId: 1, title: "Юридическая служба" },
  { id: 6, compId: 2, title: "Юридическая служба" },
  { id: 7, compId: 3, title: "Юридическая служба" },
  { id: 8, compId: 4, title: "Юридическая служба" },

  { id: 9, compId: 1, title: "Отдел кадров" },
  { id: 10, compId: 2, title: "Отдел кадров" },
  { id: 11, compId: 3, title: "Отдел кадров" },
  { id: 12, compId: 4, title: "Отдел кадров" },

  { id: 13, compId: 1, title: "Технический отдел" },
  { id: 14, compId: 2, title: "Технический отдел" },
  { id: 15, compId: 3, title: "Технический отдел" },
  { id: 16, compId: 4, title: "Технический отдел" },

  { id: 17, compId: 1, title: "Конструкторский отдел" },
  { id: 18, compId: 2, title: "Hayчно-исследовательский отдел" },
  { id: 19, compId: 3, title: "Отдел снабжения" },
  { id: 20, compId: 4, title: "Лаборатория технико-экономических исследований" }
];

export const initStateEmployers = [
  {
    id: 1,
    unitId: 1,
    surname: "Исаев",
    name: "Михаил",
    patronymic: "Александрович",
    address: " 410031, г. Саратов, ул. Первомайская, 78"
  },
  {
    id: 2,
    unitId: 2,
    surname: "Корнеев",
    name: "Антон",
    patronymic: "Владимирович",
    address: "410031, г. Саратов, ул. Первомайская,78"
  },
  {
    id: 3,
    unitId: 3,
    surname: "Никитин",
    name: "Алексей",
    patronymic: "Иванович",
    address: "410031, г. Саратов, ул. Первомайская, 78"
  },
  {
    id: 4,
    unitId: 4,
    surname: "Бусаргин",
    name: "Роман",
    patronymic: "Викторович",
    address: "410031, г.Саратов, ул.Первомайская, 78"
  },
  {
    id: 5,
    unitId: 5,
    surname: "Злобнова",
    name: "Елена",
    patronymic: "Викторовна",
    address: "410031, г. Саратов, ул. Первомайская, 78"
  },
  {
    id: 6,
    unitId: 6,
    surname: "Свиридов",
    name: "Геннадий",
    patronymic: "Александрович",
    address: "410012 г. Саратов, ул. М. Горького, 48"
  },
  {
    id: 7,
    unitId: 7,
    surname: "Свиридов",
    name: "Геннадий",
    patronymic: "Александрович",
    address: "410012 г. Саратов, ул. М. Горького, 48"
  },
  {
    id: 8,
    unitId: 8,
    surname: "Мышев",
    name: "Александр",
    patronymic: "Николаевич",
    address: "410012 г. Саратов, ул. М. Горького, 48"
  },
  {
    id: 9,
    unitId: 9,
    surname: "Струков",
    name: "Александр",
    patronymic: "Сергеевич",
    address: "410031, г. Саратов, ул. Первомайская, 78"
  },
  {
    id: 10,
    unitId: 10,
    surname: "Желанов",
    name: "Виталий",
    patronymic: "Анатольевич",
    address: "410012 г. Саратов, пр.Кирова, 29"
  },
  {
    id: 11,
    unitId: 11,
    surname: "Салеева",
    name: "Елена",
    patronymic: "Павловна",
    address: "410012, г. Саратов, Театральная площадь, 7"
  },
  {
    id: 12,
    unitId: 12,
    surname: "Ревуцкая",
    name: "Лариса",
    patronymic: "Анатольевна",
    address: "410004, г. Саратов, 2-ая Садовая, 13/19"
  },
  {
    id: 13,
    unitId: 13,
    surname: "Ревуцкая",
    name: "Лариса",
    patronymic: "Анатольевна",
    address: "410004, г. Саратов, 2-ая Садовая, 13/19"
  },
  {
    id: 14,
    unitId: 14,
    surname: "Жуковская",
    name: "Наталья",
    patronymic: "Васильевна",
    address: "410600 г. Саратов, ул. Кутякова, 11"
  },
  {
    id: 15,
    unitId: 15,
    surname: "Нефедова",
    name: "Наталья",
    patronymic: "Сергеевна",
    address: "410031, г. Саратов, ул. Первомайская, 78"
  },
  {
    id: 16,
    unitId: 11,
    surname: "Разборов",
    name: "Андрей",
    patronymic: "Андреевич",
    address: "410031, г. Саратов, ул. Первомайская, 78"
  },
  {
    id: 17,
    unitId: 17,
    surname: "Нестеренко",
    name: "Светлана",
    patronymic: "Абузяровна",
    address: "410031, г. Саратов, ул. Первомайская, 78"
  },
  {
    id: 18,
    unitId: 18,
    surname: "Кузнецов",
    name: "Николавй",
    patronymic: "Викторович",
    address: "410012, г. Саратов, ул. Московская, 88"
  },
  {
    id: 19,
    unitId: 19,
    surname: "Емельянов",
    name: "Виталий",
    patronymic: "Васильевич",
    address: "410600 г. Саратов, пр-т Кирова, 29"
  },
  {
    id: 20,
    unitId: 20,
    surname: "Овчинников",
    name: "Павел",
    patronymic: "Анатольевич",
    address: "410028 г. Саратов, ул. Московская, 59/30"
  },
  {
    id: 21,
    unitId: 1,
    surname: "Андреев",
    name: "Виталий",
    patronymic: "Александрович",
    address: " 410064, г. Саратов, ул. Тархова, 10"
  },
  {
    id: 22,
    unitId: 2,
    surname: "Нешко",
    name: "Антон",
    patronymic: "Сергеевич",
    address: "410038, г. Саратов, ул. Загороднева, 12"
  },
  {
    id: 23,
    unitId: 3,
    surname: "Никитин",
    name: "Иван",
    patronymic: "Николаевич",
    address: "410000, г. Саратов, ул. Прокатная, 11"
  },
  {
    id: 24,
    unitId: 4,
    surname: "Кошелев",
    name: "Сергей",
    patronymic: "Львович",
    address: "410001, г.Саратов, ул. Антонова, 5"
  },
  {
    id: 25,
    unitId: 5,
    surname: "Кузнецова",
    name: "Елена",
    patronymic: "Ивановна",
    address: "410081, г. Саратов, ул. Емлютина, 11"
  },
  {
    id: 26,
    unitId: 7,
    surname: "Афанасьев",
    name: "Геннадий",
    patronymic: "Александрович",
    address: "410022 г. Саратов, ул. М. Горького, 18"
  },
  {
    id: 27,
    unitId: 8,
    surname: "Столешников",
    name: "Игорь",
    patronymic: "Александрович",
    address: "410012 г. Саратов, ул. М. Горького, 18"
  },
  {
    id: 28,
    unitId: 9,
    surname: "Абрамов",
    name: "Натан",
    patronymic: "Николаевич",
    address: "410052 г. Саратов, ул. М. Лунная, 18"
  },
  {
    id: 29,
    unitId: 10,
    surname: "Стариков",
    name: "Александр",
    patronymic: "Сергеевич",
    address: "410088, г. Саратов, ул. Чехова, 2"
  },
  {
    id: 30,
    unitId: 11,
    surname: "Кривощеков",
    name: "Сергей",
    patronymic: "Анатольевич",
    address: "410052 г. Саратов, Киселева, 39"
  },
  {
    id: 31,
    unitId: 12,
    surname: "Самохвалова",
    name: "Ирина",
    patronymic: "Леонидовна",
    address: "410012, г. Саратов, Театральная площадь, 8"
  },
  {
    id: 32,
    unitId: 13,
    surname: "Слуцкая",
    name: "Ирина",
    patronymic: "Семёновна",
    address: "410064, г. Саратов, 2-ая Садовая, 9"
  },
  {
    id: 33,
    unitId: 14,
    surname: "Гетманская",
    name: "Анна",
    patronymic: "Ивановна",
    address: "410014, г. Саратов, 2-ая Прокатная, 11"
  },
  {
    id: 34,
    unitId: 15,
    surname: "Калашников",
    name: "Максим",
    patronymic: "Васильевич",
    address: "410600 г. Саратов, ул. Московская, 18"
  },
  {
    id: 35,
    unitId: 16,
    surname: "Соломон",
    name: "Лариса",
    patronymic: "Натановна",
    address: "410031, г. Саратов, ул. Первомайская, 88"
  },
  {
    id: 36,
    unitId: 17,
    surname: "Андряев",
    name: "Рушан",
    patronymic: "Андреевич",
    address: "410081, г. Саратов, ул. Молодежная, 6"
  },
  {
    id: 37,
    unitId: 18,
    surname: "Ртищева",
    name: "Елена",
    patronymic: "Абузяровна",
    address: "410081, г. Саратов, ул. Чемодурова, 20"
  },
  {
    id: 38,
    unitId: 19,
    surname: "Табаков",
    name: "Николай",
    patronymic: "Викторович",
    address: "410082, г. Саратов, ул. Московская, 50"
  },
  {
    id: 39,
    unitId: 20,
    surname: "Белов",
    name: "Виталий",
    patronymic: "Викторович",
    address: "410600 г. Саратов, пр-т Кирова, 39"
  },
  {
    id: 40,
    unitId: 1,
    surname: "Сошников",
    name: "Павел",
    patronymic: "Константинович",
    address: "410028 г. Саратов, ул. Московская, 59/15"
  }
];
