import { connect } from "react-redux";
import Employers from "../components/Employers";
import * as actionCreators from "../actions";

const mapStateToProps = state => {
  const props = {
    employers: state.employers
  };
  return props;
};

export default connect(
  mapStateToProps,
  actionCreators
)(Employers);
