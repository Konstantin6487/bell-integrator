import { connect } from "react-redux";
import AuthorizedRoute from "../components/AuthorizedRoute";
import * as actionCreators from "../actions";

const mapStateToProps = state => {
  const props = {
    login: state.login,
    loginStatus: state.login.loginStatus,
    loading: state.login.loading
  };
  return props;
};

export default connect(
  mapStateToProps,
  actionCreators
)(AuthorizedRoute);
