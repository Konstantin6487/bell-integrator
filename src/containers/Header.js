import { connect } from "react-redux";
import * as actionCreators from "../actions";
import { withRouter } from "react-router-dom";
import Header from "../components/Header";

const mapStateToProps = state => {
  const props = {
    loginStatus: state.login.loginStatus
  };
  return props;
};

export default withRouter(
  connect(
    mapStateToProps,
    actionCreators
  )(Header)
);
