import { connect } from "react-redux";
import Login from "../components/Login";
import * as actionCreators from "../actions";

const mapStateToProps = state => {
  const props = {
    login: state.login
  };
  return props;
};

export default connect(
  mapStateToProps,
  actionCreators
)(Login);
