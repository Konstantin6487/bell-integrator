import { connect } from "react-redux";
import Units from "../components/Units";
import * as actionCreators from "../actions";

const mapStateToProps = state => {
  const props = {
    units: state.units
  };
  return props;
};

export default connect(
  mapStateToProps,
  actionCreators
)(Units);
