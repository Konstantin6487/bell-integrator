import { connect } from "react-redux";
import Companies from "../components/Companies";
import * as actionCreators from "../actions";

const mapStateToProps = state => {
  const props = {
    companies: state.companies
  };
  return props;
};

export default connect(
  mapStateToProps,
  actionCreators
)(Companies);
