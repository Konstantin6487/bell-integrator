import { createAction } from "redux-actions";
import axios from "axios";

export const addUserData = createAction("USERDATA_ADD");

export const addCompany = createAction("COMPANY_ADD");
export const updateCompany = createAction("COMPANY_UPDATE");
export const removeCompany = createAction("COMPANY_REMOVE");

export const addUnit = createAction("UNIT_ADD");
export const updateUnit = createAction("UNIT_UPDATE");
export const removeUnit = createAction("UNIT_REMOVE");

export const addEmployer = createAction("EMPLOYER_ADD");
export const updateEmployer = createAction("EMPLOYER_UPDATE");
export const removeEmployer = createAction("EMPLOYER_REMOVE");

export const loginRequest = createAction("LOGIN_REQUEST");
export const loginSuccess = createAction("LOGIN_SUCCESS");
export const loginFailure = createAction("LOGIN_FAILURE");
export const loginOut = createAction("LOGIN_OUT");
//http://www.mocky.io/v2/5aafaf6f2d000057006eff31
export const getLogin = () => async dispatch => {
  dispatch(loginRequest());
  try {
    await axios("http://5b57901388d93a0014b02650.mockapi.io/bell-integrator").then(
      resp => resp.data
    );
    dispatch(loginSuccess());
  } catch (e) {
    console.log(e);
  }
};
